-- MySQL dump 10.13  Distrib 5.6.24, for Win32 (x86)
--
-- Host: localhost    Database: prueba1
-- ------------------------------------------------------
-- Server version	5.6.26-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `solicitudt`
--

DROP TABLE IF EXISTS `solicitudt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `solicitudt` (
  `idSolicitud` int(11) NOT NULL,
  `adp` varchar(45) NOT NULL,
  `ciudad` varchar(79) NOT NULL,
  `tipoT` varchar(100) NOT NULL,
  `estado` varchar(60) DEFAULT NULL,
  `identificacion` varchar(45) NOT NULL,
  PRIMARY KEY (`idSolicitud`),
  KEY `identificacion_idx` (`identificacion`),
  CONSTRAINT `identificacion` FOREIGN KEY (`identificacion`) REFERENCES `persona` (`identificacion`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `solicitudt`
--

LOCK TABLES `solicitudt` WRITE;
/*!40000 ALTER TABLE `solicitudt` DISABLE KEYS */;
INSERT INTO `solicitudt` VALUES (1,'SI','Cali','Tarjeta Oro visa','Aceptada','1013667126'),(2,'SI','Medellin','Tarjeta Clasica visa','Aceptada','123456789'),(3,'SI','Bogota','Tarjeta Oro MasterCard','Denegada','79150705'),(4,'SI','Bucaramanga','Tarjeta Platinum MasterCard','Denegada','11223344'),(5,'SI','Bucaramanga','Tarjeta Oro visa','Denegada','79150705'),(6,'SI','Bogota','Tarjeta Platinum visa','Denegada','12345656');
/*!40000 ALTER TABLE `solicitudt` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-05 22:23:31
