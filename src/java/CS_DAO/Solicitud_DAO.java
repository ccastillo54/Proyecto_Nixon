/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CS_DAO;

import CS_DTO.ClienteDTO;
import CS_DTO.Solicitud_DTO;
import CS_DTO.mostrarDTO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author camila
 */
public class Solicitud_DAO {
 

    
    public boolean crearSolicitud(Solicitud_DTO p, Connection con)
    {
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        try {            
            
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando crearSolicitud...");
            
            pstmt = con.prepareStatement("INSERT INTO solicitudt "
                    + " VALUES (?,?,?,?,?,?)");
            
            pstmt.setInt(1,p.getIdSolicitud());
            pstmt.setString(2,p.getADP());  
            pstmt.setString(3,p.getCiudad());
            pstmt.setString(4,p.getTipoT());
            pstmt.setString(5,p.getEstado());
            pstmt.setString(6,p.getIdentificacion());
            pstmt.execute();
            
            con.close();
            
            respuesta = true;
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return respuesta;

    }
//---------------------------------------------------------------------------------------------------------
     public boolean SolicitudExiste(Solicitud_DTO p, Connection con)
    {
        
        boolean respuesta = false;
        try {            
            
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando crearSolicitud..."+p.getIdentificacion());
            
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("SELECT *  FROM solicitudt WHERE identificacion='"+p.getIdentificacion()+"' AND tipoT='"+p.getTipoT()+"'");
 
              if(rs.next()){
                  respuesta=true;
              }else{
                  respuesta=false;
              }
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return respuesta;

    }
    
    //----------------------------------------------------------------------------------------------------------------------------
        public ArrayList<mostrarDTO> consultarSolicitud(Solicitud_DTO p,Connection con)
    {
        
      ArrayList<mostrarDTO> datos = new ArrayList();
        //ArrayList datos = new ArrayList<>();
        datos.clear();
        
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
       
        try {
             Statement s = con.createStatement();
                  ResultSet rs = s.executeQuery("SELECT p.nombre_1, p.nombre_2,p.apellido_1, p.apellido_2,s.idSolicitud,s.ciudad,s.tipoT,s.estado,s.identificacion,s.adp\n"
                    + "FROM prueba1.solicitudt s \n"
                    + "INNER JOIN prueba1.persona p\n"
                    + "ON s.identificacion = p.identificacion\n"
                    + "WHERE  p.identificacion='"+p.getIdentificacion()+"'");
               
            while (rs.next())
            { 
                 
                mostrarDTO per = new mostrarDTO();
                per.setNombre1(rs.getString(1));
                per.setNombre2(rs.getString(2));
                per.setApellido1(rs.getString(3));
                per.setApellido2(rs.getString(4));
                per.setIdSolicitud(rs.getInt(5));
                per.setCiudad(rs.getString(6));
                per.setTipoT(rs.getString(7));
                per.setEstado(rs.getString(8));
                per.setIdentificacion(rs.getString(9));
                per.setADP(rs.getString(10));
                datos.add(per);
                
            }
           
            
            
             Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,String.valueOf(datos.size()));
         
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
    
   //-------------------------------------------------------------------------------------------------------------------     
    
     public int obtenerIdS(String tipo,String iden,Connection con)
    {
        int id=0;
        try {
            Statement s = con.createStatement();
             ResultSet rs = s.executeQuery ("select idSolicitud from solicitudt where identificacion='"+iden+"'and tipoT='"+tipo+"' ");
            
            while (rs.next())
            { 
                id = rs.getInt(1);
            }
           

            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return id;
    }
    
  
    
    
    public int obtenerId(Connection con)
    {
        int id = -1;
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select max(idSolicitud)+1 from solicitudt");
            
            while (rs.next())
            { 
                id = rs.getInt(1);
            }
            if(id==0){
                id+=1;
            }

            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return id;
    }
         
    public boolean editarSolicitud(Solicitud_DTO p, Connection con)
    {
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        try {            
            
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando editarSolicitud...");
            
            pstmt = con.prepareStatement("UPDATE solicitudt  SET  ciudad=?,tipoT=?,estado=? WHERE idSolicitud=? AND identificacion=?");
                        
            pstmt.setString(1, p.getCiudad());
            pstmt.setString(2, p.getTipoT());
            pstmt.setString(3, p.getEstado());            
            pstmt.setInt(4,p.getIdSolicitud());
            pstmt.setString(5,p.getIdentificacion());
            pstmt.executeUpdate();
            
            con.close();
            
            respuesta = true;
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return respuesta;

    }
   
    public ArrayList<Solicitud_DTO> consultarSolicitud1( Connection con)
    {
        
        ArrayList<Solicitud_DTO> datos = new ArrayList();
        
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersonalalaal...");
        
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select idSolicitud, nombreS_1,nombreS_2, "
                    + " apellidoS_1, apellidoS_2, identificacionS, direccionS, emailS, "
                    + " telefonoS "
                    + " from solicitudt ");
                   
             
            while (rs.next())
            { 
                Solicitud_DTO per=new Solicitud_DTO();
                per.setIdSolicitud(rs.getInt(1));
               /* per.setNombre1(rs.getString(2));
                per.setNombre2(rs.getString(3));
                per.setApellido1(rs.getString(4));
                per.setApellido2(rs.getString(5));
                per.setIdentificacion(rs.getString(6));
                per.setDireccion(rs.getString(7));
                per.setEmail(rs.getString(8));
                per.setTelef(rs.getString(9));*/
               
                datos.add(per);
             // Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,per.getIdentificacion());
            }
            Solicitud_DTO p=new Solicitud_DTO();
            p.setDatos(datos);
             Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,String.valueOf(datos.size()));
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
    
    
    
      public ArrayList<ClienteDTO> consultarCliente(Connection con)
    {
        
       ArrayList<ClienteDTO> datos = new ArrayList();
        //ArrayList datos = new ArrayList<>();
        datos.clear();
        
        Logger.getLogger(ClienteDAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
       
        try {
            Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select identificacion, nombre_1,nombre_2, "
                    + " apellido_1, apellido_2, genero, telefono, email, "
                    + " fecha_nacimiento, tipo_persona, id_persona "
                    + " from persona ");
            
            while (rs.next())
            { 
               ClienteDTO per=new ClienteDTO();
                per.setIdentificacion(rs.getString(1));
                per.setNombre1(rs.getString(2));
                per.setNombre2(rs.getString(3));
                per.setApellido1(rs.getString(4));
                per.setApellido2(rs.getString(5));
                per.setGenero(rs.getString(6));
                per.setTelef(rs.getString(7));
                per.setEmail(rs.getString(8));
                per.setfNacimiento(rs.getString(9));
                per.setTipoP(rs.getString(10));
                per.setId(rs.getInt(11));
                
                datos.add(per);  
            }
            ClienteDTO per=new ClienteDTO();
            //per.setDatos(datos);
           for(int x=0;x<datos.size();x++) {
Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,per.getIdentificacion());
}
            
            
             Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,String.valueOf(datos.size()));
         
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(ClienteDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
    
       public ArrayList<Solicitud_DTO> aceptarSolicitud(Connection con)
    {
        
       ArrayList<Solicitud_DTO> datos = new ArrayList();
        //ArrayList datos = new ArrayList<>();
        datos.clear();
        
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
       
        try {
          Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select idSolicitud, adp,ciudad,tipoT,estado,identificacion "
                    + " FROM solicitudt where estado IS NULL ");
            
            while (rs.next())
            { 
                Solicitud_DTO per = new Solicitud_DTO();
                per.setIdSolicitud(rs.getInt(1));
                per.setADP(rs.getString(2));
                per.setCiudad(rs.getString(3));
                per.setTipoT(rs.getString(4));
                per.setEstado(rs.getString(5));
                per.setIdentificacion(rs.getString(6));
                datos.add(per);
                
            }
            Solicitud_DTO per=new Solicitud_DTO();
            //per.setDatos(datos);
           for(int x=0;x<datos.size();x++) {
Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,per.getIdentificacion());
}
            
            
             Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,String.valueOf(datos.size()));
         
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
    
     public boolean editarSolicitudAcep(Solicitud_DTO p, Connection con)
    {
        PreparedStatement pstmt = null;
        boolean respuesta = false;
        try {            
            
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando editarSolicitud...");
            
            pstmt = con.prepareStatement("UPDATE solicitudt "
                    + " SET "
                    + " estado=?"
                    + " where idSolicitud=?");
                        
            
            
            pstmt.setString(1, p.getEstado());
            pstmt.setInt(2,p.getIdSolicitud());
           
            
            pstmt.executeUpdate();
            
            con.close();
            
            respuesta = true;
            
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return respuesta;

    }
//------------------------------------------------------------------------------------------
     
 
      public ArrayList<mostrarDTO> ConsultaSolicitudAcep(Connection con)
    {
        
       ArrayList<mostrarDTO> datos = new ArrayList();
        //ArrayList datos = new ArrayList<>();
        datos.clear();
        
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
       
        try {
          Statement s = con.createStatement();
              ResultSet rs = s.executeQuery ("SELECT p.nombre_1, p.nombre_2,p.apellido_1, p.apellido_2,s.idSolicitud,s.ciudad,s.tipoT,s.estado,s.identificacion,s.adp FROM prueba1.solicitudt s INNER JOIN prueba1.persona p ON s.identificacion = p.identificacion WHERE  estado='Aceptada'");
                       
            
            
            while (rs.next())
            { 
                mostrarDTO per = new mostrarDTO();
                per.setNombre1(rs.getString(1));
                per.setNombre2(rs.getString(2));
                per.setApellido1(rs.getString(3));
                per.setApellido2(rs.getString(4));
                per.setIdSolicitud(rs.getInt(5));
                per.setCiudad(rs.getString(6));
                per.setTipoT(rs.getString(7));
                per.setEstado(rs.getString(8));
                per.setIdentificacion(rs.getString(9));
                 per.setADP(rs.getString(10));
                datos.add(per);
                
            }
            Solicitud_DTO per=new Solicitud_DTO();
            //per.setDatos(datos);
           for(int x=0;x<datos.size();x++) {
Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,per.getIdentificacion());
}
            
            
             Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,String.valueOf(datos.size()));
         
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
     
       public ArrayList<mostrarDTO> ConsultaSolicitudRecha(Connection con)
    {
        
       ArrayList<mostrarDTO> datos = new ArrayList();
        //ArrayList datos = new ArrayList<>();
        datos.clear();
        
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
       
        try {
          Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("SELECT p.nombre_1, p.nombre_2,p.apellido_1, p.apellido_2,s.idSolicitud,s.ciudad,s.tipoT,s.estado,s.identificacion,s.adp FROM prueba1.solicitudt s INNER JOIN prueba1.persona p ON s.identificacion = p.identificacion WHERE  estado='Denegada'");
                       
            
            
            while (rs.next())
            { 
                mostrarDTO per = new mostrarDTO();
                per.setNombre1(rs.getString(1));
                per.setNombre2(rs.getString(2));
                per.setApellido1(rs.getString(3));
                per.setApellido2(rs.getString(4));
                per.setIdSolicitud(rs.getInt(5));
                per.setCiudad(rs.getString(6));
                per.setTipoT(rs.getString(7));
                per.setEstado(rs.getString(8));
                per.setIdentificacion(rs.getString(9));
                 per.setADP(rs.getString(10));
                datos.add(per);
                
                
                
                
                
            }
            Solicitud_DTO per=new Solicitud_DTO();
            //per.setDatos(datos);
           for(int x=0;x<datos.size();x++) {
Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,per.getIdentificacion());
}
            
            
             Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,String.valueOf(datos.size()));
         
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
   
             public ArrayList<Solicitud_DTO> ConSolicitud(Solicitud_DTO p ,Connection con)
    {
        
       ArrayList<Solicitud_DTO> datos = new ArrayList();
        //ArrayList datos = new ArrayList<>();
        datos.clear();
        
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
       
        try {
          Statement s = con.createStatement();
            ResultSet rs = s.executeQuery ("select idSolicitud, adp,ciudad,tipoT,estado,identificacion "
                    + " FROM solicitudt where idSolicitud='"+p.getIdSolicitud()+"' ");
            
            while (rs.next())
            { 
                Solicitud_DTO per = new Solicitud_DTO();
                per.setIdSolicitud(rs.getInt(1));
                per.setADP(rs.getString(2));
                per.setCiudad(rs.getString(3));
                per.setTipoT(rs.getString(4));
                per.setEstado(rs.getString(5));
                per.setIdentificacion(rs.getString(6));
                datos.add(per);
                
            }
      
            
             Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,String.valueOf(datos.size()));
         
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }
    //----------------------------------------------------------------------------   
   
    public ArrayList<mostrarDTO> ConsultaSolicitudClien(Connection con)
    {
        
      ArrayList<mostrarDTO> datos = new ArrayList();
        //ArrayList datos = new ArrayList<>();
        datos.clear();
        
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando consultarPersona...");
       
        try {
            Statement s = con.createStatement();
                  ResultSet rs = s.executeQuery("SELECT p.nombre_1, p.nombre_2,p.apellido_1, p.apellido_2,s.idSolicitud,s.ciudad,s.tipoT,s.estado,s.identificacion\n"
                    + "FROM prueba1.solicitudt s \n"
                    + "INNER JOIN prueba1.persona p\n"
                    + "ON s.identificacion = p.identificacion\n"
                    + "WHERE  s.idSolicitud");
               
            while (rs.next())
            { 
                 
                mostrarDTO per = new mostrarDTO();
                per.setNombre1(rs.getString(1));
                per.setNombre2(rs.getString(2));
                per.setApellido1(rs.getString(3));
                per.setApellido2(rs.getString(4));
                per.setIdSolicitud(rs.getInt(5));
                per.setCiudad(rs.getString(6));
                per.setTipoT(rs.getString(7));
                per.setEstado(rs.getString(8));
                per.setIdentificacion(rs.getString(9));
                datos.add(per);
                
            }
            Solicitud_DTO per=new Solicitud_DTO();
            //per.setDatos(datos);
           for(int x=0;x<datos.size();x++) {
Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,per.getIdentificacion());
}
            
            
             Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO,String.valueOf(datos.size()));
         
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return datos;
    }           
             
             
             
}
