/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;


import CS_DAO.ClienteDAO;
import CS_DAO.Solicitud_DAO;
import CS_DTO.ClienteDTO;
import CS_DTO.Solicitud_DTO;
import CS_DTO.mostrarDTO;
import co.edu.uniminuto.pa.bds.MySqlDataSource;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

/**
 *
 * @author camila
 */
@Controller
@RequestMapping("/")
public class SolicitudController {
    
    
    
      @RequestMapping("SolicitudCrear.htm")
    public String processSubmit2crear(HttpServletRequest req,SessionStatus status,ModelMap model) 
    {
        
        Solicitud_DAO pDao = new Solicitud_DAO();
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit3...");       
       ClienteDTO p = new ClienteDTO();
         List<ClienteDTO> datos= pDao.consultarCliente( MySqlDataSource.getConexionBD());
       // List<ClienteDTO> datos =p.getDatos();
        //Logger.getLogger(SolicitudController.class.getName()).log(Level.SEVERE, null, "Consultar + " + "-" + datos.size());
        
        model.put("listaSolicitud", datos);
        if (datos.size() > 0)
            model.put("mensaje", "La consulta se realizo satisfactoriamente!!!" + datos.size());
        else
            model.put("mensaje", "La consulta NO tiene resultados...");
        
        return "Solicitud_crear";
    }
    //---------------------------------------------------------------------------------------------------------
        @RequestMapping(method = RequestMethod.GET, value = "SolicitudDigita.htm")
    public String processSubmit(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

        System.out.println("SolicitudCrear");
        model.put("mensajeSolicitud", "Pase por el controller de Solicitud:::"+req.getParameter("nombre"));
        return "Solicitud_digita";
    }    

@RequestMapping(method = RequestMethod.POST, value = "SolicitudRegistrar.htm")
    public String processSubmit1(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {
        

        Solicitud_DAO pDao = new Solicitud_DAO();
            
        Logger.getLogger(SolicitudController.class.getName()).log(Level.INFO, "Ejecutando processSubmit1...");

       int id = pDao.obtenerId(MySqlDataSource.getConexionBD());
        String adp = req.getParameter("ADP");
        String ciudad = req.getParameter("ciudad");
        String tipot = req.getParameter("tipoT");
        ClienteDTO c=new ClienteDTO();
        String identi=req.getParameter("identificacion");

        Solicitud_DTO p = new Solicitud_DTO();
        p.setIdSolicitud(id);
        p.setADP(adp);
        p.setCiudad(ciudad);
        p.setTipoT(tipot);
        p.setIdentificacion(identi);
        Logger.getLogger(SolicitudController.class.getName()).log(Level.SEVERE, null, "Registrar + " + "-" +p.getIdentificacion() );
      
        if(p.getIdentificacion().equalsIgnoreCase("null")){
            model.put("mensaje", "La identificación no puede estar vacía, por favor dirigirse a crear Solicitud"); 
        }else{
        boolean existe = pDao.SolicitudExiste(p, MySqlDataSource.getConexionBD());
         if (existe == false) {
        boolean insert = pDao.crearSolicitud(p, MySqlDataSource.getConexionBD());

        Logger.getLogger(SolicitudController.class.getName()).log(Level.SEVERE, null, "Registrar + " + "-" + insert);
        
        if (insert)
            model.put("mensaje", "La Solicitud fue creada satisfactoriamente!!!");
        else
            model.put("mensaje", "La Solicitud NO fue creada, consulte con el administrador...");
         }else{
            model.put("mensaje", "Usted ya tiene una solicitud registrada para"+p.getTipoT()+", Porfavor verifique su estado.");  
         }
        }
        return "Solicitud_digita";
    }     

@RequestMapping(method = RequestMethod.GET, value = "SolicitudEditar.htm")
    public String processSubmit4(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {      
        Logger.getLogger(SolicitudController.class.getName()).log(Level.INFO, "Ejecutando processSubmit4...");
        return "Solicitud_editar";
    } 
    
@RequestMapping(method = RequestMethod.POST, value = "SolicitudEditarForm1.htm")
    public String processSubmit5(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

       Solicitud_DAO pDao = new Solicitud_DAO();
        
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit5...");

        
        String ident = req.getParameter("identificacion");
        String tipoT = req.getParameter("tipoT");
        int id = pDao.obtenerIdS(tipoT,ident,MySqlDataSource.getConexionBD()); 
      Solicitud_DTO p = new Solicitud_DTO(); 
        p.setIdSolicitud(id);
        p.setTipoT(tipoT);
        p.setIdentificacion(ident);
        List<Solicitud_DTO> datos = pDao.ConSolicitud(p, MySqlDataSource.getConexionBD());

        Logger.getLogger(SolicitudController.class.getName()).log(Level.SEVERE, null, "Consultar + " + ident + "-" + datos.size());
        
        model.put("listaSolicitud", datos);
        
        
        return "Solicitud_editar";
        
    }    
    @RequestMapping(method = RequestMethod.POST, value = "SolicitudEditarForm2.htm")
    public String processSubmit6(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

       Solicitud_DAO pDao = new Solicitud_DAO();
        Solicitud_DTO u = new Solicitud_DTO();
            
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit6...");
        String var = req.getParameter("tipo");
        String estado=req.getParameter("estado");
        String ciudad=req.getParameter("ciudad");
        String ident = req.getParameter("identificacion");
        String tipoT = req.getParameter("tipoT");
        int id = pDao.obtenerIdS(var,ident,MySqlDataSource.getConexionBD()); 
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Id Solicitud: " + id+" "+estado+" "+tipoT+" "+var);
        
       Solicitud_DTO p = new Solicitud_DTO();
       p.setIdSolicitud(id);
       p.setTipoT(tipoT);
       p.setCiudad(ciudad);
       p.setEstado(estado);
       p.setIdentificacion(ident);
  
        boolean res = pDao.editarSolicitud(p, MySqlDataSource.getConexionBD());                         
        
        if (res)
            model.put("mensaje", "Se edito satisfactoriamente!!!");
        else
            model.put("mensaje", "NO se guardaron los cambios...");
        
        return "Solicitud_editar";
        
    }  
    
    
    //-----------------------------------------------------------------------------------------------------------
       @RequestMapping("SolicitudAcep.htm")
    public String processSubmit20crear(HttpServletRequest req,SessionStatus status,ModelMap model) 
    {
        
        Solicitud_DAO pDao = new Solicitud_DAO();
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit20...");       
      Solicitud_DTO p = new Solicitud_DTO();
         List<Solicitud_DTO> datos= pDao.aceptarSolicitud(MySqlDataSource.getConexionBD());
       // List<ClienteDTO> datos =p.getDatos();
        //Logger.getLogger(SolicitudController.class.getName()).log(Level.SEVERE, null, "Consultar + " + "-" + datos.size());
        
        model.put("listaSolicitud", datos);
        if (datos.size() > 0)
            model.put("mensaje", "--> "+ datos.size()+" Registros");
        else
            model.put("mensaje", "No hay solicitudes que aprobar...");
        
        return "Solicitud_acep";
    }
    
    //----------------------------------------------------------------------------------------------------------
    
    
        @RequestMapping(method = RequestMethod.GET, value = "SolicitudAf.htm")
    public String processSubmit34(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

        System.out.println("SolicitudCrear");
        model.put("mensajeSolicitud", "Pase por el controller de Solicitud:::"+req.getParameter("nombre"));
        return "Solicitud_af";
    }    


    @RequestMapping(method = RequestMethod.POST, value = "SolicitudAceptar.htm")
    public String processSubmit62(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

       Solicitud_DAO pDao = new Solicitud_DAO();
        
            
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit6...");

        
        String ident =req.getParameter("identificacion");
        String tipo=req.getParameter("tipoT");
        String estado = req.getParameter("Estado");
        int id = pDao.obtenerIdS(tipo,ident,MySqlDataSource.getConexionBD()); 
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Id Solicitud: " + ident+" "+tipo+" "+id);
        
       Solicitud_DTO p = new Solicitud_DTO();
       p.setIdSolicitud(id);
       p.setIdentificacion(ident);
       p.setTipoT(tipo);
       p.setEstado(estado);
  
        boolean res = pDao.editarSolicitudAcep(p, MySqlDataSource.getConexionBD());                         
        
        if (res)
            model.put("mensaje", "Las Solicitudes fueron  Editadas");
        else
            model.put("mensaje", "Las Solicitudes fueron no Editadas");
        
       return "Solicitud_acep";
        
    }     
    
      @RequestMapping("SolicitudAceptada.htm")
    public String processSubmit201crear(HttpServletRequest req,SessionStatus status,ModelMap model) 
    {
        
        Solicitud_DAO pDao = new Solicitud_DAO();
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit20...");       
      Solicitud_DTO p = new Solicitud_DTO();
         List<mostrarDTO> datos= pDao.ConsultaSolicitudAcep(MySqlDataSource.getConexionBD());
       // List<ClienteDTO> datos =p.getDatos();
        //Logger.getLogger(SolicitudController.class.getName()).log(Level.SEVERE, null, "Consultar + " + "-" + datos.size());
        
        model.put("listaSolicitud", datos);
        if (datos.size() > 0)
            model.put("mensaje", " # de Solicitudes Aceptadas:"+ datos.size());
        else
            model.put("mensaje", "No hay solicitudes Aceptadas...");
        
        return "Solicitud_aceptada";
    }
    
    
       
      @RequestMapping("SolicitudRechazada.htm")
    public String processSubmit24(HttpServletRequest req,SessionStatus status,ModelMap model) 
    {
        
        Solicitud_DAO pDao = new Solicitud_DAO();
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit20...");       
      Solicitud_DTO p = new Solicitud_DTO();
         List<mostrarDTO> datos= pDao.ConsultaSolicitudRecha(MySqlDataSource.getConexionBD());
       // List<ClienteDTO> datos =p.getDatos();
        //Logger.getLogger(SolicitudController.class.getName()).log(Level.SEVERE, null, "Consultar + " + "-" + datos.size());
        
        model.put("listaSolicitud", datos);
        if (datos.size() > 0)
            model.put("mensaje", " # de Solicitudes Rechazadas:"+ datos.size());
        else
            model.put("mensaje", "No hay solicitudes Rechazadas...");
        
        return "Solicitud_rechazada";
    }
    
        @RequestMapping("SolicitudPcliente.htm")
    public String processSubmit21(HttpServletRequest req,SessionStatus status,ModelMap model) 
    {
        
        Solicitud_DAO pDao = new Solicitud_DAO();
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit20...");       
      Solicitud_DTO p = new Solicitud_DTO();
         List<mostrarDTO> datos= pDao.ConsultaSolicitudClien(MySqlDataSource.getConexionBD());
       // List<ClienteDTO> datos =p.getDatos();
        //Logger.getLogger(SolicitudController.class.getName()).log(Level.SEVERE, null, "Consultar + " + "-" + datos.size());
        
        model.put("listaSolicitud", datos);
        if (datos.size() > 0)
            model.put("mensaje", " # de Solicitudes Totales:"+ datos.size());
        else
            model.put("mensaje", "No hay solicitudes ...");
        
        return "Solicitud_Pcliente";
    }
 //----------------------------------------------------------------------------------------------------------
       
@RequestMapping(method = RequestMethod.GET, value = "SolicitudConsultar.htm")
    public String processSubmit2(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {      
        Logger.getLogger(ClienteController.class.getName()).log(Level.INFO, "Ejecutando processSubmit2...");
        return "Solicitud_consultar";
    } 
    
@RequestMapping(method = RequestMethod.POST, value = "SolicitudConsultarForm.htm")
    public String processSubmit3(HttpServletRequest req, SessionStatus status,ModelMap model) 
    {

        Solicitud_DAO pDao = new Solicitud_DAO();
        
            
        Logger.getLogger(Solicitud_DAO.class.getName()).log(Level.INFO, "Ejecutando processSubmit3...");

        int id = pDao.obtenerId(MySqlDataSource.getConexionBD());
        String ident = req.getParameter("identificacion");
        String nombre1 = req.getParameter("nombre1");
        
       Solicitud_DTO p = new Solicitud_DTO();
        p.setIdSolicitud(id);
        p.setIdentificacion(ident);

            
        List<mostrarDTO> datos = pDao.consultarSolicitud(p, MySqlDataSource.getConexionBD());

        Logger.getLogger(ClienteController.class.getName()).log(Level.SEVERE, null, "Consultar + " + ident + "-" + datos.size());
        
        model.put("listaSolicitud1", datos);
        if (datos.size() > 0)
            model.put("mensaje", "La consulta se realizo satisfactoriamente!!!" + datos.size());
        else
            model.put("mensaje", "La consulta NO tiene resultados...");
        
        return "Solicitud_consultar";
    }   
    
    
     
    
}
